import Component from '@ember/component';

export default Component.extend({
  hello: true,

  actions: {
    switch: function() {
      if (this.set('hello', false)) {
        this.set('hello', true);
      }
      else {
        this.set('hello', false);
      }
    }
  }
});
