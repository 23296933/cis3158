import DS from 'ember-data';

export default DS.Model.extend({
  username: DS.attr('string'),
  e-mail_address: DS.attr('string'),
  age: DS.attr('number')
});
